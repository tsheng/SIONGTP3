package com.example.tp3.myapplication3;

import android.os.AsyncTask;
import com.example.myapplication3.City;
import com.example.myapplication3.WebServiceUrl;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class TaskBackground extends AsyncTask<String , Integer, City> {
        //pas au point sur l'asyncTask
        private City city;
    @Override
    protected void doInBackground(String... str) {
        HttpURLConnection url = null;
        try{
            URL urlCo = WebServiceUrl.build(city.getName(), city.getCountry());
            HttpURLConnection urlConnection = (HttpURLConnection) urlCo.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            url.disconnect();
        }
        return city;


    }

}
