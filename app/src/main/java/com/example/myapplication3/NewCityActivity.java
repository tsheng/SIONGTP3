package com.example.myapplication3;
import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewCityActivity extends AppCompatActivity {

    public static String Ville = "NewVille";//on va recupérer la novelle ville dans le mainActivity
    private EditText textName, textCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String nom=(String) textName.getText().toString();
                String country=(String) textCountry.getText().toString();
                City city = new City(nom, country);
                Intent intent = new Intent();
                intent.putExtra(Ville, city);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }




}