package com.example.myapplication3;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int FIRST_ACTIVITY_REQUEST_CODE = 1;
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 2;
    static String villeMaj;
    private WeatherDbHelper dbHelper;
    private SimpleCursorAdapter cursorAdapter;
    Cursor myCursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Mettre en argument l'id dans le content_main

        final ListView list = (ListView) findViewById(R.id.listitem);
        /*
        final City Avignon=(City)new City("Avignon","France");
        City Carpentras=(City)new City("Carpentras","France");
        City Orange=(City)new City("Orange","France");
        City Beda=(City)new City("Bedarrides","France");
        City Monteux=(City)new City("Monteux","France");
        */
        //créé bdd avec populate() qui sera afficher dans le content_main
        dbHelper = new WeatherDbHelper(this);
        myCursor=dbHelper.fetchAllCities();
        dbHelper.populate();

        cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllCities(),
                new String[]{WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.
                        COLUMN_COUNTRY},
                new int[]{android.R.id.text1, android.R.id.text2});

        list.setAdapter(cursorAdapter);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                City city = dbHelper.cursorToCity(cursor);
                Toast.makeText(MainActivity.this, "Tu as cliqué sur la ville: " + city, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                intent.putExtra("Ville", city);
                startActivityForResult(intent, FIRST_ACTIVITY_REQUEST_CODE);
            }
        });

        //button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
            super.onActivityResult(requestCode, resultCode, data);

        //MAJ de la ville
        if (FIRST_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            City updatedCity = data.getParcelableExtra(MainActivity.villeMaj);
            dbHelper.updateCity(updatedCity);
            myCursor = dbHelper.fetchAllCities();
            cursorAdapter.changeCursor(myCursor);
            cursorAdapter.notifyDataSetChanged();
        }
        //creation d'une nouvelle ville
        if (SECOND_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            City newCity = data.getParcelableExtra(NewCityActivity.Ville);
            dbHelper.addCity(newCity);
            myCursor = dbHelper.fetchAllCities();
            cursorAdapter.changeCursor(myCursor);
            cursorAdapter.notifyDataSetChanged();
        }


        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
